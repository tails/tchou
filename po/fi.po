# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# karvjorm <karvonen.jorma@gmail.com>, 2014
# Tomi Toivio <tomi@sange.fi>, 2013
# tonttula, 2013
# Finland355 <ville.ehrukainen2@gmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-01-14 16:01+0100\n"
"PO-Revision-Date: 2014-12-04 08:50+0000\n"
"Last-Translator: runasand <runa.sandvik@gmail.com>\n"
"Language-Team: Finnish (http://www.transifex.com/projects/p/torproject/"
"language/fi/)\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready-notification.sh:42
msgid "Tor is ready"
msgstr "Tor on valmis"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready-notification.sh:43
msgid "You can now access the Internet."
msgstr "Voit nyt päästä internettiin."

#: config/chroot_local-includes/etc/whisperback/config.py:64
#, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>If you don't mind disclosing some bits of your identity\n"
"to Tails developers, you can provide an email address to\n"
"let us ask more details about the bug. Additionally entering\n"
"a public PGP key enables us to encrypt such future\n"
"communication.</p>\n"
"<p>Anyone who can see this reply will probably infer you are\n"
"a Tails user. Time to wonder how much you trust your\n"
"Internet and mailbox providers?</p>\n"
msgstr ""
"<h1>Auta meitä korjaamaan tämä ohjelmointivirhe!</h1>\n"
"<p>Lue <a href=\"%s\">vikailmoitusohjeemme</a>.</p>\n"
"<p><strong>Älä sisällytä henkilökohtaisia tietoja enempää kuin on "
"tarpeellista!</strong></p>\n"
"<h2>Sähköpostiosoitteen antamisesta meille</h2>\n"
"<p>Jos et pelkää henkilöllisyydestäsi paljastamista Tails-kehittäjille, voit "
"tarjota sähköpostiosoitteen, josta voimme kysyä lisätietoja viasta. Lisäksi "
"julkisen PGP-avaimen kirjoittaminen sallii meidän salakirjoittaa sellaisen "
"tulevan yhteydenpidon.</p>\n"
"<p>Kaikki, jotka voivat nähdä tämän vastauksen päättelevät luultavasti, että "
"olet Tails-käyttäjä. Aika miettiä, kuinka paljon luotat Internettiisi ja "
"sähköpostipalvelutarjoajiisi?</p>\n"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:136
msgid "OpenPGP encryption applet"
msgstr "OpenPGP-salakirjoitussovelma"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:139
msgid "Exit"
msgstr "Lopeta"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:141
msgid "About"
msgstr "Tietoa"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:192
msgid "Encrypt Clipboard with _Passphrase"
msgstr "Salakirjoita leikepöytä _Salasanalla"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:195
msgid "Sign/Encrypt Clipboard with Public _Keys"
msgstr "Allekirjoita/salakirjoita leikepöytä julkisilla _avaimilla"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:200
msgid "_Decrypt/Verify Clipboard"
msgstr "_Pura/todenna leikepöydän salaus"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:204
msgid "_Manage Keys"
msgstr "_Hallitse avaimia"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:244
msgid "The clipboard does not contain valid input data."
msgstr "Leikepöytä ei sisällä kelvollista syötetietoa."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:295
#: config/chroot_local-includes/usr/local/bin/gpgApplet:297
#: config/chroot_local-includes/usr/local/bin/gpgApplet:299
msgid "Unknown Trust"
msgstr "Tuntematon luotettavuus."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:301
msgid "Marginal Trust"
msgstr "Marginaalinen luottamus"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:303
msgid "Full Trust"
msgstr "Täysi luotettavuus."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:305
msgid "Ultimate Trust"
msgstr "Rajoittamaton luottamus"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:358
msgid "Name"
msgstr "Nimi"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:359
msgid "Key ID"
msgstr "Avaintunniste"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:360
msgid "Status"
msgstr "Tila"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:392
msgid "Fingerprint:"
msgstr "Sormenjälki:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:395
msgid "User ID:"
msgid_plural "User IDs:"
msgstr[0] "Käyttäjätunniste:"
msgstr[1] "Käyttäjätunnisteet:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:425
msgid "None (Don't sign)"
msgstr "Ei mitään (älä allekirjoita)"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:488
msgid "Select recipients:"
msgstr "Valitse vastaanottajat:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:496
msgid "Hide recipients"
msgstr "Piilota vastaanottajat"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:499
msgid ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."
msgstr ""
"Piilota salakirjoitetun viestin kaikkien vastaanottajien käyttäjätunnisteet. "
"Muuten kaikki, jotka näkevät salakirjoitetun viestin, voivat nähdä ketkä "
"ovat vastaanottajat."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:505
msgid "Sign message as:"
msgstr "Allekirjoita viesti nimellä:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:509
msgid "Choose keys"
msgstr "Valitse avaimet"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:549
msgid "Do you trust these keys?"
msgstr "Luotatko näihin avaimiin?"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:552
msgid "The following selected key is not fully trusted:"
msgid_plural "The following selected keys are not fully trusted:"
msgstr[0] "Seuraavaan valittuun avaimeen ei lueteta täysin:"
msgstr[1] "Seuraaviin valittuihin avaimiin ei luoteta täysin:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:570
msgid "Do you trust this key enough to use it anyway?"
msgid_plural "Do you trust these keys enough to use them anyway?"
msgstr[0] "Luotatko tähän avaimeen kylliksi käyttääksesi sitä silti?"
msgstr[1] "Luotatko näihin avaimiin kylliksi käyttääksesi niitä silti?"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:583
msgid "No keys selected"
msgstr "Ei valittuja avaimia"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:585
msgid ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."
msgstr ""
"Sinun on valittava yksityinen avain viestin allekirjoitukseen, tai jonkun "
"julkisista avaimista viestin salakirjoittamiseen, tai molemmat."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:613
msgid "No keys available"
msgstr "Avaimia ei saatavilla"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:615
msgid ""
"You need a private key to sign messages or a public key to encrypt messages."
msgstr ""
"Tarvitset yksityisen avaimen viestin allekirjoitukseen tai julkisen avaimen "
"viestin salakirjoittamiseen."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:743
msgid "GnuPG error"
msgstr "GnuPG-virhe"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:764
msgid "Therefore the operation cannot be performed."
msgstr "Tämän vuoksi tehtävää ei voida suorittaa."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:814
msgid "GnuPG results"
msgstr "GnuPG-tulokset"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:820
msgid "Output of GnuPG:"
msgstr "GnuPG-tuloste:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:845
msgid "Other messages provided by GnuPG:"
msgstr "Muut GnuPG-viestit:"

#: config/chroot_local-includes/usr/local/lib/shutdown-helper-applet:39
msgid "Shutdown Immediately"
msgstr "Sammuta välittömästi"

#: config/chroot_local-includes/usr/local/lib/shutdown-helper-applet:40
msgid "Reboot Immediately"
msgstr "Käynnistä uudelleen välittömästi"

#: config/chroot_local-includes/usr/local/bin/tails-about:16
msgid "not available"
msgstr "ei saatavilla"

#: config/chroot_local-includes/usr/local/bin/tails-about:19
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr "Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:24
msgid "The Amnesic Incognito Live System"
msgstr "Amnesic Incognito Live-järjestelmä"

#: config/chroot_local-includes/usr/local/bin/tails-about:25
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""
"Rakentamistiedot:\n"
"%s"

#: config/chroot_local-includes/usr/local/bin/tails-about:27
#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:1
msgid "About Tails"
msgstr "Tails-ohjelmasta"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:118
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:124
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:128
msgid "Your additional software"
msgstr "Lisäohjelmistosi"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:119
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:129
msgid ""
"The upgrade failed. This might be due to a network problem. Please check "
"your network connection, try to restart Tails, or read the system log to "
"understand better the problem."
msgstr ""
"Päivitys epäonnistui. Tämä saattaa johtua verkkopulmasta. Tarkista "
"verkkoyhteytesi, yritä käynnistää Tails uudelleen, tai lue järjestelmäloki "
"ymmärtääksesi pulman paremmin."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:125
msgid "The upgrade was successful."
msgstr "Päivitys onnistui."

#: config/chroot_local-includes/usr/local/bin/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr "Synkronoidaan järjestelmän kelloa"

#: config/chroot_local-includes/usr/local/bin/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""
"Tor tarvitsee tarkan kellon toimiakseen oikein, erityisesti palvelujen "
"piilottamiseen. Odota hetkinen..."

#: config/chroot_local-includes/usr/local/bin/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr "Kellon synkronointi epäonnistui!"

#: config/chroot_local-includes/usr/local/sbin/tails-restricted-network-detector:38
msgid "Network connection blocked?"
msgstr "Verkkoyhteys estetty?"

#: config/chroot_local-includes/usr/local/sbin/tails-restricted-network-detector:40
msgid ""
"It looks like you are blocked from the network. This may be related to the "
"MAC spoofing feature. For more information, see the <a href=\\\"file:///usr/"
"share/doc/tails/website/doc/first_steps/startup_options/mac_spoofing.en."
"html#blocked\\\">MAC spoofing documentation</a>."
msgstr ""
"Näyttää siltä kuin sinut olisi eristetty verkosta. Tämä saattaa johtua MAC-"
"osoitteen väärennysominaisuudesta. Katso lisätietoja osoitteesta <a href=\\"
"\"file:///usr/share/doc/tails/website/doc/first_steps/startup_options/"
"mac_spoofing.en.html#blocked\\\">MAC-väärennysohjeet</a>."

#: config/chroot_local-includes/usr/local/bin/tails-security-check:145
msgid "This version of Tails has known security issues:"
msgstr "Tässä Tails-versiossa on tunnettuja turvallisuusriskejä:"

#: config/chroot_local-includes/usr/local/sbin/tails-spoof-mac:37
#, sh-format
msgid "Network card ${nic} disabled"
msgstr "Verkkokortti ${nic} otettu pois käytöstä"

#: config/chroot_local-includes/usr/local/sbin/tails-spoof-mac:38
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing. See the <a "
"href='file:///usr/share/doc/tails/website/doc/first_steps/startup_options/"
"mac_spoofing.en.html'>documentation</a>."
msgstr ""
"MAC-osoitteen väärennös epäonnistui verkkokortilla ${nic_name} (${nic}), "
"joten se on otettu tilapäisesti pois käytöstä.\n"
"Olisi ehkä paras käynnistää Tails uudelleen ja ottaa pois käytöstä MAC-"
"väärennös. Katso <a href='file:///usr/share/doc/tails/website/doc/"
"first_steps/startup_options/mac_spoofing.en.html'>ohjeet</a>."

#: config/chroot_local-includes/usr/local/sbin/tails-spoof-mac:47
msgid "All networking disabled"
msgstr "Kaikki verkkoyhteydet otettu pois käytöstä"

#: config/chroot_local-includes/usr/local/sbin/tails-spoof-mac:48
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing. See the <a "
"href='file:///usr/share/doc/first_steps/startup_options/mac_spoofing.en."
"html'>documentation</a>."
msgstr ""
"MAC-osoitteen väärennös epäonnistui verkkokortilla ${nic_name} (${nic}). "
"Myös virhepalautuminen epäonnistui, joten kaikki verkkoyhteydet on otettu "
"pois käytöstä.\n"
"Olisi ehkä paras käynnistää Tails uudelleen ja ottaa pois käytöstä MAC-"
"väärennös. Katso <a href='file:///usr/share/doc/first_steps/startup_options/"
"mac_spoofing.en.html'>ohjeet</a>."

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:19
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:61
msgid "error:"
msgstr "virhe:"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:20
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:62
msgid "Error"
msgstr "Virhe"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:40
msgid ""
"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual"
msgstr ""
"<b>Muistia ei ole kylliksi päivitysten tarkistamiseksi.</b>\n"
"\n"
"Varmista, että järjestelmä tyydyttää suoritettavan Tails-ohjelman "
"vaatimukset.\n"
"Katso file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Yritä käynnistää Tails uudelleen päivitysten tarkistamiseksi.\n"
"\n"
"Tai tee manuaalinen päivitys.\n"
"Katso https://tails.boum.org/doc/first_steps/upgrade#manual"

#: config/chroot_local-includes/usr/local/bin/tails-virt-notify-user:53
msgid "Warning: virtual machine detected!"
msgstr "Varoitus: virtuaalikone havaittu!"

#: config/chroot_local-includes/usr/local/bin/tails-virt-notify-user:55
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails."
msgstr ""
"Sekä isäntäkäyttöjärjestelmä että virtualisointiohjelmisto kykenevät "
"valvomaan, mitä teet Tails-ohjelmassa."

#: config/chroot_local-includes/usr/local/bin/tails-virt-notify-user:57
msgid ""
"<a href='file:///usr/share/doc/tails/website/doc/advanced_topics/"
"virtualization.en.html'>Learn more...</a>"
msgstr ""
"<a href='file:///usr/share/doc/tails/website/doc/advanced_topics/"
"virtualization.en.html'>Opi lisää...</a>"

#: config/chroot_local-includes/usr/local/bin/tor-browser:18
msgid "Tor is not ready"
msgstr "Tor ei ole valmis"

#: config/chroot_local-includes/usr/local/bin/tor-browser:19
msgid "Tor is not ready. Start Tor Browser anyway?"
msgstr "Tor ei ole valmis. Käynnistetäänkö Tor-selain siitä huolimatta?"

#: config/chroot_local-includes/usr/local/bin/tor-browser:20
msgid "Start Tor Browser"
msgstr "Käynnistä Tor-selain"

#: config/chroot_local-includes/usr/local/bin/tor-browser:21
msgid "Cancel"
msgstr "Peruuta"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:72
msgid "Do you really want to launch the Unsafe Browser?"
msgstr "Haluatko varmasti käynnistää turvattoman selaimen?"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:74
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>. Only use "
"the Unsafe Browser if necessary, for example if you have to login or "
"register to activate your Internet connection."
msgstr ""
"Verkkotoiminta turvattomalla webbiselaimella <b>ei ole anonyymia</b>. Käytä "
"turvatonta webbiselainta vain kun se on välttämätöntä, esimerkiksi jos sinun "
"on kirjauduttava tai rekisteröidyttävä Internet-yhteytesi aktivoimiseksi."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:75
msgid "_Launch"
msgstr "_Käynnistä"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:76
msgid "_Exit"
msgstr "_Lopeta"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:86
msgid "Starting the Unsafe Browser..."
msgstr "Käynnistetään turvaton selain..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:87
msgid "This may take a while, so please be patient."
msgstr "Tämä voi kestää hetken, odota rauhassa."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:121
msgid "Failed to setup chroot."
msgstr "Chroot-asennus epäonnistui."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:195
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr "Turvaton selain"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:251
msgid "Shutting down the Unsafe Browser..."
msgstr "Suljetaan turvaton selain..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:252
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""
"Tämä saattaa kestää hetkisen, ja et saa käynnistää turvatonta webbiselainta "
"uudelleen, kunnes se on suljettu oikein."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:264
msgid "Failed to restart Tor."
msgstr "Torin uudelleenkäynnistys epäonnistui."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:272
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""
"Toinen turvaton webbiselain on käynnissä, tai sitä ollaan sulkemassa. Yritä "
"uudelleen hetken kuluttua."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:285
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""
"DHCP:stä tai manuaalisesti asetetusta Verkkohallinnasta ei saatu DNS-"
"palvelinta."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:30
msgid "I2P failed to start"
msgstr "I2P-käynnistys epäonnistui"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:31
msgid ""
"Something went wrong when I2P was starting. Check the logs in /var/log/i2p "
"for more information."
msgstr ""
"Jotain meni pieleen I2P:n käynnistyksen yhteydessä. Tarkista lisätietoja "
"lokitiedostosta osoitteessa /var/log/i2p."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:42
msgid "I2P's router console is ready"
msgstr "I2P:n reititinpääteikkuna on valmis"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:43
msgid "You can now access I2P's router console on http://127.0.0.1:7657."
msgstr "Pääset I2P:n reititinpääteikkunaan osoitteessa http://127.0.0.1:7657."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:48
msgid "I2P is not ready"
msgstr "I2P ei ole valmis"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:49
msgid ""
"Eepsite tunnel not built within six minutes. Check the router console at "
"http://127.0.0.1:7657/logs or the logs in /var/log/i2p for more information. "
"Reconnect to the network to try again."
msgstr ""
"Eepsite-tunnelia ei ole koostettu kuuden minuutin aikana. Tarkista "
"lisätietoja reititinpääteikkunasta osoitteessa http://127.0.0.1:7657/logs "
"tai lokitiedostoista osoitteessa /var/log/i2p. Yritä uudelleen kytkeytymällä "
"taas verkkoon."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:59
msgid "I2P is ready"
msgstr "I2P on valmis"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:60
msgid "You can now access services on I2P."
msgstr "Sinulla on nyt pääsy I2P-palveluihin."

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_an_error.desktop.in.h:1
msgid "Report an error"
msgstr "Ilmoita virheestä"

#: ../config/chroot_local-includes/etc/skel/Desktop/tails-documentation.desktop.in.h:1
#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr "Tails-ohjeet"

#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:2
msgid "Learn how to use Tails"
msgstr "Opi kuinka käyttää Tails-ohjelmaa."

#: ../config/chroot_local-includes/usr/share/applications/i2p-browser.desktop.in.h:1
msgid "Anonymous overlay network browser"
msgstr "Anonyymi peiteverkkoselain"

#: ../config/chroot_local-includes/usr/share/applications/i2p-browser.desktop.in.h:2
msgid "I2P Browser"
msgstr "I2P-selain"

#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:2
msgid "Learn more about Tails"
msgstr "Opi lisää Tails-ohjelmasta"

#: ../config/chroot_local-includes/usr/share/applications/tails-reboot.desktop.in.h:1
msgid "Reboot"
msgstr "Käynnistä uudelleen"

#: ../config/chroot_local-includes/usr/share/applications/tails-reboot.desktop.in.h:2
msgid "Immediately reboot computer"
msgstr "Käynnistä tietokone uudelleen välittömästi"

#: ../config/chroot_local-includes/usr/share/applications/tails-shutdown.desktop.in.h:1
msgid "Power Off"
msgstr "Sammuta"

#: ../config/chroot_local-includes/usr/share/applications/tails-shutdown.desktop.in.h:2
msgid "Immediately shut down computer"
msgstr "Sammuta tietokone välittömästi"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:1
msgid "Tor Browser"
msgstr "Tor-selain"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:2
msgid "Anonymous Web Browser"
msgstr "Anonyymi webbiselain"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr "Selaa webbimaailmaa ilman anonymiteettiä"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr "Turvaton Web-selain"

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr "Tails-kohtaiset työkalut"

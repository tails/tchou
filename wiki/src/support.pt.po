# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-04-15 12:14+0300\n"
"PO-Revision-Date: 2014-06-17 11:30-0300\n"
"Last-Translator: Tails Developers <amnesia@boum.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Support\"]]\n"
msgstr "[[!meta title=\"Suporte\"]]\n"

#. type: Title =
#, no-wrap
msgid "Search the documentation\n"
msgstr "Buscar na documentação\n"

#. type: Plain text
msgid "Read the [[official documentation|doc]] to learn more about how Tails works and maybe start answering your questions. It contains:"
msgstr "Leia a [[documentação oficial|doc]] para saber mais sobre como o Tails funciona e talvez começar a responder suas perguntas. Ela contém:"

#. type: Bullet: '  - '
msgid "General information about what Tails is"
msgstr "Informações gerais sobre o que é o Tails"

#. type: Bullet: '  - '
msgid "Information to understand how it can help you and what its limitations are"
msgstr "Informações para você saber como o Tails pode te ajudar e quais são suas limitações"

#. type: Bullet: '  - '
msgid "Guides covering typical uses of Tails"
msgstr "Guias que explicam os usos típicos do Tails"

#. type: Plain text
msgid "[[Visit Tails documentation|doc]]"
msgstr "[[Visite a documentação|doc]]"

#. type: Title =
#, no-wrap
msgid "Frequently asked questions\n"
msgstr "Perguntas frequentes\n"

#. type: Plain text
msgid "Search our list of [[frequently asked questions|faq]]."
msgstr "Busque en nossa lista de [[perguntas frequentes|faq]]."

#. type: Title =
#, no-wrap
msgid "Upgrade\n"
msgstr "Atualização\n"

#. type: Plain text
msgid "Make sure you are using the latest version, as [[upgrading|doc/first_steps/upgrade]] might solve your problem."
msgstr "Tenha certeza de que você está usando a última versão, pois [[atualizar|doc/first_steps/upgrade]] pode resolver o seu problema."

#. type: Title =
#, no-wrap
msgid "Check if the problem is already known\n"
msgstr "Verifique se seu problema já é conhecido\n"

#. type: Plain text
msgid "You can have a look at:"
msgstr "Você pode dar uma olhada:"

#. type: Bullet: '  - '
msgid "The [[list of known issues|support/known_issues]]"
msgstr "Na [[lista de problemas conhecidos|support/known_issues]]"

#. type: Bullet: '  - '
msgid "The [[list of things to do|todo]]"
msgstr "Na [[lista de coisas a fazer|todo]]"

#. type: Bullet: '  - '
msgid "The [list of things that will be in the next release](https://labs.riseup.net/code/projects/tails/issues?query_id=111)"
msgstr "Na [lista de coisas que serão incluídas na próxima versão](https://labs.riseup.net/code/projects/tails/issues?query_id=111)"

#. type: Plain text
#, no-wrap
msgid "<div id=\"page-found_a_problem\">\n"
msgstr "<div id=\"page-found_a_problem\">\n"

#. type: Plain text
#, no-wrap
msgid "<div id=\"bugs\">\n"
msgstr "<div id=\"bugs\">\n"

#. type: Plain text
#, no-wrap
msgid "  <h1>Report an error</h1>\n"
msgstr "  <h1>Relate um erro</h1>\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>If you are facing an error in Tails, please follow the [[bug reporting\n"
"  guidelines|doc/first_steps/bug_reporting]].</p>\n"
msgstr ""
"  <p>Se você encontrou um erro no Tails, por favor siga as [[diretivas de\n"
"  relatamento de bugs|doc/first_steps/bug_reporting]].</p>\n"

#. type: Plain text
#, no-wrap
msgid ""
"  <p>If Tails does not start, please see our specific\n"
"  [[reporting guidelines|doc/first_steps/bug_reporting/tails_does_not_start]].</p>\n"
msgstr ""
"  <p>Se o Tails não estiver iniciando, por favor veja nossa página específica sobre\n"
"  [[diretivas de relatamento|doc/first_steps/bug_reporting/tails_does_not_start]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div> <!-- #bugs -->\n"
msgstr "</div> <!-- #bugs -->\n"

#. type: Plain text
#, no-wrap
msgid "<div id=\"wishlist\">\n"
msgstr "<div id=\"wishlist\">\n"

#. type: Plain text
#, no-wrap
msgid "  <h1>Request a feature</h1>\n"
msgstr "  <h1>Peça uma funcionalidade</h1>\n"

#. type: Plain text
#, no-wrap
msgid ""
"  If you would like to see a new feature in Tails,\n"
"  search the [[!tails_redmine desc=\"open tickets in Redmine\"]] first,\n"
"  and file a new ticket in there if no existing one matches your needs.\n"
msgstr ""
"  Se você quiser ver uma nova funcionalidade no Tails,\n"
"  faça uma busca nos [[!tails_redmine desc=\"tíquetes abertos no Redmine\"]]\n"
"  primeiro, e abra um novo tíquete lá se não existir um que satisfaça suas necessidades.\n"

#. type: Plain text
#, no-wrap
msgid "</div> <!-- #wishlist -->\n"
msgstr "</div> <!-- #wishlist -->\n"

#. type: Plain text
#, no-wrap
msgid "</div> <!-- #page-found_a_problem -->\n"
msgstr "</div> <!-- #page-found_a_problem -->\n"

#. type: Plain text
#, no-wrap
msgid "<div id=\"talk\">\n"
msgstr "<div id=\"talk\">\n"

#. type: Plain text
#, no-wrap
msgid "  <h1>Get in touch with us</h1>\n"
msgstr "  <h1>Entre em contato</h1>\n"

#. type: Plain text
#, no-wrap
msgid "  [[!inline pages=\"support/talk\" raw=\"yes\"]]\n"
msgstr "  [[!inline pages=\"support/talk.pt\" raw=\"yes\"]]\n"

#. type: Plain text
#, no-wrap
msgid "</div> <!-- #talk -->\n"
msgstr "</div> <!-- #talk -->\n"

#~ msgid "How-tos on getting Tails to work"
#~ msgstr "How-tos sobre como fazer o Tails funcionar"

#~ msgid "It contains:"
#~ msgstr "Conteúdo:"

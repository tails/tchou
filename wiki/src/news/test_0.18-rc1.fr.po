# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2014-09-22 12:11+0300\n"
"PO-Revision-Date: 2013-05-07 14:20-0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta date=\"2013-05-05 13:45:00\"]] [[!meta title=\"Call for testing: "
"0.18~rc1\"]]"
msgstr ""
"[[!meta date=\"2013-05-05 13:45:00\"]] [[!meta title=\"Appel à tester Tails "
"0.18~rc1\"]]"

#. type: Content of: <p>
msgid ""
"You can help Tails! The first (and hopefully only) release candidate for the "
"upcoming version 0.18 is out. Please test it and see if all works for you."
msgstr ""
"Vous pouvez aider Tails ! La première (et on espère seule) version candidate "
"pour la version 0.18 à venir est sortie. Merci de la tester et de voir si "
"tout fonctionne pour vous."

#. type: Content of: outside any tag (error?)
msgid "[[!toc levels=1]]"
msgstr ""

#. type: Content of: <h1>
msgid "How to test Tails 0.18~rc1?"
msgstr "Comment tester Tails 0.18~rc1?"

#. type: Content of: <ol><li><p>
msgid ""
"<strong>Keep in mind that this is a test image.</strong> We have made sure "
"that it is not broken in an obvious way, but it might still contain "
"undiscovered issues."
msgstr ""
"Gardez à l'esprit que c'est une image de test. Nous nous sommes assuré "
"qu'elle n'est pas corrompue d'une manière évidente, mais elle peut toujours "
"contenir des problèmes non découverts."

#. type: Content of: <ol><li><p>
msgid "Download the ISO image and its signature:"
msgstr "Téléchargez l'image ISO et sa signature :"

#. type: Content of: <ol><li><p>
msgid ""
"<a class=\"download-file\" href=\"http://dl.amnesia.boum.org/tails/testing/"
"tails-i386-0.18-rc1/tails-i386-0.18-rc1.iso\" >Tails 0.18~rc1 ISO image</a>"
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"<a class=\"download-signature\" href=\"http://dl.amnesia.boum.org/tails/"
"testing/tails-i386-0.18-rc1/tails-i386-0.18-rc1.iso.pgp\" >Tails 0.18~rc1 "
"signature</a>"
msgstr ""

#. type: Content of: <ol><li><p>
msgid "[[Verify the ISO image|download#verify]]."
msgstr "[[Vérifier l'image ISO|download#verify]]."

#. type: Content of: <ol><li><p>
msgid ""
"Have a look at the list of <a href=\"#known_issues\">known issues of this "
"release</a> and the list of [[longstanding known issues|support/"
"known_issues]]."
msgstr ""
"Jetez un œil à la liste des <a href=\"#known_issues\">problèmes connus de "
"cette version</a> et à la liste des [[problèmes connus de longue date|"
"support/known_issues]]."

#. type: Content of: <ol><li><p>
msgid "Test wildly!"
msgstr "Testez sauvagement !"

#. type: Content of: <p>
msgid ""
"If you find anything that is not working as it should, please [[report to us|"
"doc/first_steps/bug_reporting]]! Bonus points if you check that it is not a "
"<a href=\"#known_issues\">known issue of this release</a> or a "
"[[longstanding known issue|support/known_issues]]."
msgstr ""
"Si vous découvrez quelquechose qui ne fonctionne pas comme prévu, merci de "
"[[nous le rapporter|doc/first_steps/bug_reporting]]! Points bonus si vous "
"vérifiez que ce n'est pas un <a href=\"#known_issues\">problème connu de "
"cette version</a> ou un [[problème connu de longue date|support/"
"known_issues]]."

#. type: Content of: <h1>
msgid "What's new since 0.17.2?"
msgstr "Quoi de neuf depuis la 0.17.2 ?"

#. type: Content of: <ul><li>
msgid "New features"
msgstr "Nouvelles fonctionnalités"

#. type: Content of: <ul><ul><li><p>
msgid "Support obfs3 bridges."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid ""
"Automatically install a custom list of additional packages chosen by the "
"user at the beginning of every working session, and upgrade them once a "
"network connection is established (technology preview)."
msgstr ""

#. type: Content of: <ul><li>
msgid "Iceweasel"
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Update Torbrowser patches to current maint-2.4 branch (567682b)."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Isolate DOM storage to first party URI, and enable DOM storage."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Isolate the image cache per url bar domain."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Torbutton 1.5.2, and various prefs hacks to fix breakage."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "HTTPS Everywhere 3.2."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid ""
"Update prefs to match the TBB's, fix bugs, and take advantage of the latest "
"Torbrowser patches."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Make prefs organization closer to the TBB's."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Cleanup prefs."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Update unsafe browser prefs mangling accordingly."
msgstr ""

#. type: Content of: <ul><li>
msgid "Bugfixes"
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Fixed swapped filenames of tails-{reboot,shutdown}.desktop."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Only add ClientTransportPlugin to torrc when bridge mode is enabled."
msgstr ""

#. type: Content of: <ul><li><p>
msgid "Minor improvements"
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid ""
"Set <code>kernel.dmesg_restrict=1</code>, and make <code>/proc/&lt;pid&gt;/</"
"code> invisible and restricted for other users."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Install gnome-screenshot."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Don't disable IPv6 on all network interfaces anymore."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Add a \"About Tails\" launcher in the System menu."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Install GNOME accessibility themes."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid ""
"Use <em>Getting started...</em> as the homepage for Tails documentation "
"button."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid ""
"Stop relying on the obsolete <code>/live/image</code> compatibility symlink."
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Disable audio preview in Nautilus."
msgstr ""

#. type: Content of: <ul><li><p>
msgid "Localization: many translation updates all over the place."
msgstr ""

#. type: Content of: <ul><li><p>
msgid "Build process"
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Make Vagrant's build-tails script support Jenkins too."
msgstr ""

#. type: Content of: <ul><li><p>
msgid "Test suite"
msgstr ""

#. type: Content of: <ul><ul><li><p>
msgid "Fix Unsafe Browser test broken by hidepid."
msgstr ""

#. type: Content of: <h1>
msgid "<a id=\"known_issues\"></a>Known issues in 0.18~rc1"
msgstr "<a id=\"known_issues\"></a>Problèmes connus dans la version 0.18~rc1"

#. type: Content of: <ul><li>
msgid ""
"A number of Iceweasel prefs are not applied. This can result in a more "
"fingerprintable web browsing behaviour than usual."
msgstr ""

#. type: Content of: <ul><li>
msgid ""
"The Iceweasel web browser leaks some browser type information via JavaScript."
msgstr ""

#. type: Content of: <p>
msgid "All these issues should be fixed in Tails 0.18."
msgstr ""

[[!meta title="Automated builds specification"]]


This blueprint helps to keep track of the discussion on the mailing list, and
is attached to tickets **#8655** to specify how to implement **#6196** (Build all
active branches).

[[!toc levels=2]]

# Question to discuss

## Which branches we want to build?

We already build the base branches (_stable_, _testing_, _devel_ and
_experimental_) + _feature/jessie_.

The questions raised is mostly concern the _feature/*_ and _bugfix/*_ branches
(so _topic branches_)

Some metrics about the number of branches merged per release could give hints
that might help to decide of selection process.

Proposal1:

* branches which are not merged into devel, stable and testing
* but had new commits since the previous release


We should probably also let devs being able to trigger automated builds for a
branch that would not be selected by the previous algo (eg. last commit too
old).
Devs could do so that by dropping a timestamp file in their branch. The
branch would be added to the selection if this timestamp is smaller than a
certain amount of to-be-defined time.


## When to build it

Define the regularity we want to build topic branches, apart from being build
on git pushes.

As with the first question, some metrics could help the discussion,
at least having an average number of branches per release.

Note that we will have to plug that in automatic tests when they will be
deployed.

Proposal 1: A build a day.


## Notifications

When to notify who? And how to notify them?

Proposal 1: Notify by email the author of the offending commit on failure.


# Scenarios

In the folowing scenario:

0. topic branches are named branch T
0. base branches are named branch B
0. builds are ran on merges which don't raise a conflict. If the merge raises a conflict, then the topic branch's developpes should take care of resolving it.


## Scenario 1 : reviewer

    As a reviewer
    When I'm asked to review branch T into branch B
    Then I need to know if branch T builds fine
      once merged into branch B (fresh results!)
    And I should be notified of the results
    And if the build succeeded
      I might want to download the resulting ISO
      I might want to get the pkg list
      I want the redmine ticket to be notified (optional feature)
    Otherwise if it fails the developer who proposed the merge should be notified
      And the developper _need_ to see the build logs
      And the ticket should be reassigned to the branch submitter
      And QA check should be set to `Dev needed`


## Scenario 2 : developer

    As a developper who has the commit bit
    When I'm working on branch T
    Then I need to know if my branch builds after I've pushed
    And I need to know if my branch build is broken by something else
       possibly weeks after my last commit (by e.g Debian changes,
       changes in branch B, ...)
    And if the build succeeded
      I might want to download the resulting ISO
      I might want to get the pkg list
      I want the redmine ticket to be notified (optional feature)
    Otherwise if it fails I _need_ to see the build logs
      And the developer who proposed the merge should be notified
      And the ticket should be reassigned to the branch submitter
      And QA check should be set to `Dev needed`


## Scenario 3 : RM

    As the current RM
    When working the full dev release cycle
    Then I need to know when a branch FTBFS
    And when this happens I need to see the build logs.


# Future ideas

This list other scenarios not part of the first deployement iteration, but we
might want to consider it in the future.

## Scenario 1

    As a Tails developer working on branch B
    When I upload a package to APT suite B
    Then I want to know if it broke the build ASAP

(same responsiveness as when pushing to git)
(acceptable workaround: being able to manually trigger a build.)


## Scenario 2

    As the current RM
    When I push new tag T on branch B
    Then I want the APT suite for tag T to be created
    And I want the APT suite B to be copied into the APT suite T
    And once this is done, I want a build from the checkout of tag T to be
      triggered
    And I want the squashfs sort file to be generated, and the diff sent to me


## Scenario 3

    As a Tails developper
    When the test suite is ran on the ISO build from my last commit
    I want to watch the TV and see the test video in HTML5 from a Torbrowser


## Scenario 4

    As a Tails developper
    When an ISO is build from my last commit
    I want to access it throught remote desktop (VNC/Spice/...) over Tor

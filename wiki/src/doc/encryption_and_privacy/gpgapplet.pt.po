# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-03-06 23:43+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails OpenPGP Applet\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Tails includes a custom applet, called <span\n"
"class=\"application\">Tails OpenPGP Applet</span>, to manipulate text using\n"
"OpenPGP.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/encryption_and_privacy/gpgapplet.warning\" raw=\"yes\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<span class=\"application\">Tails OpenPGP Applet</span> is located in the notification area.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"[[!img gpgapplet_with_text.png link=no alt=\"Tails OpenPGP Applet with lines of\n"
"text\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "With <span class=\"application\">Tails OpenPGP Applet</span> you can:\n"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Encrypt text with a passphrase|encryption_and_privacy/gpgapplet/"
"passphrase_encryption]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Encrypt and sign text with a public key|encryption_and_privacy/gpgapplet/"
"public-key_cryptography]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"[[Decrypt and verify text|encryption_and_privacy/gpgapplet/decrypt_verify]]"
msgstr ""

#. type: Plain text
msgid ""
"Note that the applet doesn't manage your keys, that is done by Seahorse."
msgstr ""

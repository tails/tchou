[[!meta title="Calendar"]]

* 2015-02-03: [[Monthly meeting|contribute/meetings]]

* 2015-02-11:
  - Feature freeze for Tails 1.3.
  - Translation window starts for Tails 1.3.
  - Build and upload Tails 1.3~rc1 ISO image and IUKs.
  - Start testing Tails 1.3~rc1 during late CET?

* 2015-02-12:
  - Finish testing Tails 1.3~rc1 by the afternoon, CET.
  - Release Tails 1.3~rc1 during late CET.

* 2015-02-12: [[Low-hanging fruits session|contribute/low-hanging_fruit_sessions]]

* 2015-02-23:
  - All translations and bug fixes targeting Tails 1.3 must be merged
    into the 'testing' branch.
  - Tor Browser 4.x, based on Firefox 31.5.0esr is *hopefully* out.
  - Build and upload Tails 1.3 ISO image and IUKs.
  - Start testing Tails 1.3 during late CET?

* 2015-02-24:
  - Finish testing Tails 1.3 by the afternoon, CET.
  - Release Tails 1.3 during late CET.

* 2015-03-03: [[Monthly meeting|contribute/meetings]]

* 2015-03-12: [[Low-hanging fruits session|contribute/low-hanging_fruit_sessions]]

* 2015-04-07: Release 1.3.1.

* 2015-05-19: Release 1.4.

* 2015-06-30: Release 1.4.1

* 2015-08-11: Release 1.5

* 2015-09-22: Release 1.5.1

* 2015-11-03: Release 1.6

* 2015-12-22: Release 1.6.1

* 2016-02-02: Release 1.7

* 2016-03-15: Release 1.7.1
